package com.flashfuse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.flashfuse.activity.admin.AdminHomeActivity;
import com.flashfuse.activity.LoginActivity;
import com.flashfuse.activity.UserHomeActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("LoginState", MODE_PRIVATE);
        boolean isLoggedIn = sharedPreferences.getBoolean("isLoggedIn", false);
        String role = sharedPreferences.getString("role", "");

        if (isLoggedIn) {
            if (role.equals("admin")) {
                // Chuyển đến AdminActivity
                Intent intent = new Intent(MainActivity.this, AdminHomeActivity.class);
                startActivity(intent);
                finish();
            } else {
                // Chuyển đến UserActivity
                Intent intent = new Intent(MainActivity.this, UserHomeActivity.class);
                startActivity(intent);
                finish();
            }
        } else {
            // Chuyển đến LoginActivity
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}