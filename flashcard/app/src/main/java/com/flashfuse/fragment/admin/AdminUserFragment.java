package com.flashfuse.fragment.admin;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.flashfuse.R;
import com.flashfuse.adapter.UserRecycleAdapter;
import com.flashfuse.data.dao.UserDAO;
import com.flashfuse.data.entity.User;

import java.util.List;

public class AdminUserFragment extends Fragment {
    EditText search_user;
    Button search_button, btn_create_user;
    UserRecycleAdapter adapter;
    List<User> list;
    RecyclerView recyclerView;
    UserDAO userDAO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_admin_user, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.search_results);
        search_user = view.findViewById(R.id.search_user);
        search_button = view.findViewById(R.id.search_button);
//        btn_create_user= view.findViewById(R.id.btn_create_user);

        userDAO = new UserDAO(getContext());
        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list = userDAO.searchUser(search_user.getText().toString());
                adapter = new UserRecycleAdapter(getContext(), list );
                adapter.setList(list);
                recyclerView.setAdapter(adapter);
                LinearLayoutManager manager= new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                recyclerView.setLayoutManager(manager);
            }
        });
//        btn_create_user.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }

}