package com.flashfuse.fragment.admin;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.flashfuse.R;
import com.flashfuse.adapter.DeckFullRecycleViewAdapter;
import com.flashfuse.adapter.DeckRecycleViewAdapter;
import com.flashfuse.data.dao.DeckDAO;
import com.flashfuse.data.entity.Deck;

import java.util.ArrayList;
import java.util.List;

public class AdminDeckFragment extends Fragment {

    EditText search_deck;
    Button search_button;
    RecyclerView search_results;
    DeckDAO deckDAO;
    List<Deck> list;

    DeckFullRecycleViewAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_admin_deck, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        search_deck = view.findViewById(R.id.search_deck);
        search_button = view.findViewById(R.id.search_button);
        search_results = view.findViewById(R.id.search_results);
        deckDAO = new DeckDAO(this.getContext());
        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deckName = search_deck.getText().toString();
                List<Deck> decks = deckDAO.getDeckByName(deckName);

                adapter= new DeckFullRecycleViewAdapter(getContext(), decks);
                search_results.setAdapter(adapter);
                LinearLayoutManager manager= new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                search_results.setLayoutManager(manager);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        list = new ArrayList<>();
        adapter= new DeckFullRecycleViewAdapter(getContext(), list);
        search_results.setAdapter(adapter);
        LinearLayoutManager manager= new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        search_results.setLayoutManager(manager);
    }
}