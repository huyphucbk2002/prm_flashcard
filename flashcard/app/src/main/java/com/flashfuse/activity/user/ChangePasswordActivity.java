package com.flashfuse.activity.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flashfuse.R;
import com.flashfuse.data.dao.UserDAO;
import com.flashfuse.data.entity.User;

public class ChangePasswordActivity extends AppCompatActivity {

    EditText current_password, new_password, confirm_password;
    Button btn_change, btn_cancel;

    UserDAO userDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        current_password = findViewById(R.id.current_password);
        new_password = findViewById(R.id.new_password);
        confirm_password = findViewById(R.id.confirm_password);
        btn_change = findViewById(R.id.btn_change);
        btn_cancel = findViewById(R.id.btn_cancel);
        userDAO = new UserDAO(this);

        SharedPreferences sharedPreferences = getSharedPreferences("LoginState", MODE_PRIVATE);
        int userId = sharedPreferences.getInt("userId", 0);
        User user = userDAO.getUserById(userId);

        btn_change.setOnClickListener(v -> {
            String currentPassword = current_password.getText().toString();
            String newPassword = new_password.getText().toString();
            String confirmPassword = confirm_password.getText().toString();
            if (currentPassword.equals(user.getPassword())) {
                if (newPassword.equals(confirmPassword)) {
                    user.setPassword(newPassword);
                    userDAO.updateUser(user);
                    Toast.makeText(this, "Password has been changed", Toast.LENGTH_SHORT).show();
                    finish();
                } else{
                    Toast.makeText(this, "New password and confirm password do not match", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Current password is incorrect", Toast.LENGTH_SHORT).show();
            }
        });
    }
}