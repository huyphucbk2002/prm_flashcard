package com.flashfuse.activity.card;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.flashfuse.R;
import com.flashfuse.data.dao.CardDAO;
import com.flashfuse.data.entity.Card;

public class EditCardActivity extends AppCompatActivity {

    EditText et_deck_name, et_deck_description;
    Button bt_save, bt_cancel;

    Card card;

    CardDAO cardDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_card);

        et_deck_name = findViewById(R.id.et_deck_name);
        et_deck_description = findViewById(R.id.et_deck_description);
        bt_save = findViewById(R.id.btn_save);
        bt_cancel = findViewById(R.id.btn_cancel);
        cardDAO = new CardDAO(this);

        Intent intent = getIntent();
        if(intent.getSerializableExtra("card") != null){
            card = (Card) intent.getSerializableExtra("card");
        }
        System.out.println(card.toString());

        et_deck_name.setText(card.getFront());
        et_deck_description.setText(card.getBack());

        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Save card
                card.setFront(et_deck_name.getText().toString());
                card.setBack(et_deck_description.getText().toString());

                cardDAO.updateCard(card);
                finish();
            }
        });

        bt_cancel.setOnClickListener(v -> {
            finish();
        });

    }
}