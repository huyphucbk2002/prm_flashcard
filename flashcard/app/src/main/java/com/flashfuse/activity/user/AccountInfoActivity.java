package com.flashfuse.activity.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.flashfuse.R;
import com.flashfuse.data.dao.UserDAO;
import com.flashfuse.data.entity.User;

public class AccountInfoActivity extends AppCompatActivity {
    TextView usernameTextView, name, emailTextView, phone;
    Button btn_edit_profile, btn_change_password, btn_cancel;

    UserDAO userDAO;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_info);

        usernameTextView = findViewById(R.id.usernameTextView);
        name = findViewById(R.id.name);
        emailTextView = findViewById(R.id.emailTextView);
        phone = findViewById(R.id.phone);
        btn_edit_profile = findViewById(R.id.btn_edit_profile);
        btn_change_password = findViewById(R.id.btn_change_password);
        btn_cancel = findViewById(R.id.btn_cancel);
        userDAO = new UserDAO(this);

        //get user info
        SharedPreferences sharedPreferences = getSharedPreferences("LoginState", MODE_PRIVATE);
        int userId = sharedPreferences.getInt("userId", 0);
        User user = userDAO.getUserById(userId);

        usernameTextView.setText(user.getUsername());
        name.setText(user.getName());
        emailTextView.setText(user.getEmail());
        phone.setText(user.getPhone());

        btn_edit_profile.setOnClickListener(v -> {
            Intent intent = new Intent(this, EditProfileActivity.class);
            startActivity(intent);
        });

        btn_change_password.setOnClickListener(v -> {
            Intent intent = new Intent(this, ChangePasswordActivity.class);
            startActivity(intent);
        });

        btn_cancel.setOnClickListener(v -> {
            finish();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("LoginState", MODE_PRIVATE);
        int userId = sharedPreferences.getInt("userId", 0);
        user = userDAO.getUserById(userId);
        usernameTextView.setText(user.getUsername());
        name.setText(user.getName());
        emailTextView.setText(user.getEmail());
        phone.setText(user.getPhone());
    }
}